import pdb
import unittest
import power.power as proj

class TestPower(unittest.TestCase):

    def test_count_matches_simple_0(self):
        """Simple match. Most common
        """
        aline = "abc hello abc, this not counted: defabcxyz"
        # The power word 'abc' gets decorated:
        pat_abc = 'abc'
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 2

    def test_count_matches_simple_1(self):
        """Simple match. Most common
        Leading power word not counted if it is a prefix.
        """

        aline = "abc hello abc, this not abccounted: defabcxyz"
        # The power word 'abc' gets decorated:
        pat_abc = r"abc"
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 2

    def test_count_matches_simple_2(self):
        """Simple match. Most common
        Leading power word not counted if it is a prefix.
        """

        aline = "abc hello abc, this notabc, but ignore qqqabczzz"
        # The power word 'abc' gets decorated:
        pat_abc = "abc"
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 2

    def test_count_matches_lines_0(self):
        """Counts in multiple lines.
        """

        aline = """This is abc as embedded in abc
        stuff of abcdream it is not. This
        sentence is abc nonsense."""
        # The power word 'abc' gets decorated:
        pat_abc = "abc"
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 3

    def test_count_lines_caps_0(self):
        """Counts in multiple lines. Uses caps.
        """

        aline = """This is Abc as embedded in abc
        stuff of abcdream it is not. This
        sentence is abc nonsense.""".lower()
        # The power word 'abc' gets decorated:
        pat_abc = "abc"
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 3

    def test_spaces_in_power_word_0(self):
        """Counts in multiple lines. Power word
        has embedded spaces.
        """

        aline = """This is wreaking havoc as embedded in abc
        stuff of abcdream it is not. This
        sentence is wreaking havoc.""".lower()
        # The power word 'abc' gets decorated:
        pat_abc = "wreaking havoc"
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 2

    def test_hyphen_in_power_word_0(self):
        """Counts in multiple lines. Power word
        has embedded hyphens.
        """

        aline = """This is life-changing as embedded in abc
        stuff of life changing dream it is not. This
        sentence is life-changing.""".lower()
        # The power word 'abc' gets decorated:
        pat_abc = "life-changing"
        cnt = proj.count_matches(pat_abc, aline)
        assert cnt == 2

    def test_reading_file_0(self):
        """Reads a file and pulls metrics from it."""
        file_name = "power_words_1.dat"
        argv = ['python3', '--input', file_name]
        proj.main(argv)


if __name__ == "__main__":
    unittest.main()
        
