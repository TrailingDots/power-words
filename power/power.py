#!/bin/env python3
"""
Given a user text document, scan the document
and report on all the power words in that document.

The power word database gets encoded as python
structs to avoid reading a file. This allows easier
distribution as one and only one file has everything.
"""
import argparse
import os
import re
import sys

class PowerWordClass():
    """A class of power words like fear, desire, ...
    This class reads the power word text file and
    builds an internal dictionary that gets used
    to construct metrics from a user document."""
    def __init__(self, name, description, words):
        # The name of this power word class
        self.name = name
        # The description of this class
        self.description = description
        # A list of power words for this class
        self.words = words
        # A count of the number of hits for this class
        self.hits = 0

    def __str__(self):
        max_words_displayed = 5
        # Only these words will be displayed
        words_shortened = self.words[:max_words_displayed]
        out = f"<PowerWordClass> name={self.name}," + \
              f"description={self.description},"  + \
              f"words={words_shortened}...," + \
              f"hits={self.hits}"
        return out


class AllPowerWords():
    """Class that handles all the classes of power words.

    A single power word class looks like:
    fear = PowerWordClass(name, description, words[])

    """
    def __init__(self, all_power_word_classes):
        # List of all power word classes
        self.all_power_word_classes = all_power_word_classes

        # Dictionary of all power words
        # key = power word.
        # value = counts of this word in user input.
        self.power_words_dict = {}

        # Maps power_word to class
        # key = Power word
        # value = Name of class of power word, e.g.: "fear"
        self.word_to_class = {}

        # Populate word_to_class that maps a word to a
        # specific power class name.
        # Upon finishing the loop, use as:
        # power_words.word_to_class['agony']
        # 'Provoke Fear'
        # power_words_dict.power_words['agony'] now equals 0
        # power_words_dict.power_words['agony'] += 1
        # power_words_dict.power_words['agony'] now equals 1
        for aclass in self.all_power_word_classes:
            self.add_power_word_class(aclass)

        # Power word dictionary
        # key = class name
        # value = the power word class
        self.name_to_class = {}
        for aclass in self.all_power_word_classes:
            self.name_to_class[aclass.name] = aclass

    def add_power_word_class(self, a_power_word_class):
        """Append a new power_word_class with all the power words
        input:
            power_word_class = A PowerWordClass fully populated
        """
        #print(f"Adding: {a_power_word_class.name}")

        # Add these words to the power_words dict
        adict = {aword.lower(): 0 for aword in a_power_word_class.words}
        self.power_words_dict.update(adict)

        # Add the class of each word to the word_to_class dict
        for aword in a_power_word_class.words:
            self.word_to_class[aword.lower()] = a_power_word_class.name


class Report():
    """Produces all reports.
    """

    def __init__(self, power_words, user_arg_dict, parser, sorted_counted):
        """
        power_words - a class of AllPowerWords
        user_arg_dict - dictionary from user args
        parser - parsed user args
        sorted_counted - sorted list of (word, count) of non-zero counts
        """
        self.power_words = power_words
        self.user_arg_dict = user_arg_dict
        self.parser = parser
        self.sorted_counted = sorted_counted

    def sorted_counted_to_str(self):
        """Converted the sorted_counted to a readable
        string and return that.

        sorted_counted look like:
            [('bloodbath', 3), ('assault', 2), ('blinded', 2), ...]
        A list of tuples. Each tuple has a power word and a count.

        The output is  count, power_word, class  formatted.
        """
        def val_key_str(kv_tuple):
            out = f"{kv_tuple[1]:5}" + \
                " " + \
                f"{kv_tuple[0]:12}" +\
                " " + \
                self.power_words.word_to_class[kv_tuple[0]]
            return out
        vk_list = [val_key_str(kv_tuple) for kv_tuple in self.sorted_counted]
        indent = " " * 4
        vk_str = indent + ("\n" + indent).join(vk_list)
        return vk_str

    def all_power_words_count(self):
        """Count the number of power words and report to user."""
        cnt = len(self.power_words.power_words_dict)
        out = f"\n=== All Power Words by class - {cnt} total power words ===\n"

        sorted_str = self.sorted_counted_to_str()
        out += sorted_str
        return out

    def summary_by_class(self):
        """Provide a summary by class for all classes
        with counts.

        Returns:
            list of tuplesof class names and counts.
            Reports even zero counts.
            Tuple:
                (class name, counts for this input, total words this class)
        """
        out_list = []
        for aclass in self.power_words.all_power_word_classes:
            name = aclass.name
            words = aclass.words
            # Determine the counts this user input
            the_counts = aclass.hits
            #(class name, counts for this input, total words this class)
            out_list.append((name, the_counts, len(words)))
        return out_list

    def print_summary_per_class(self, counts_per_class):
        """Print the results of self.summary_by_class
        counts_per_class is a tupleand looks like:
            (class name, counts for this input, total words this class)
        """
        indent = " "*4
        for item in counts_per_class:
            print(f"{indent} {item[1]:4} of {item[2]:4}" + \
                    "\t" + \
                    f"{item[0]}")

    def power_words_by_class(self):
        """From the sorted counted list, classify each power word
        by class.
        Return the string of this counted classifications.
        """
        # Create new list augmented by class for each word.
        vkc = [(val, key, self.power_words.word_to_class[key])
            for (key, val) in self.sorted_counted]

        # Classify each according to class.
        # classes key is the class name.
        # classes value is a sorted list of power words in that class.
        classes = {}
        for tup in vkc:
            if tup[2] not in classes:
                classes[tup[2]] = []
            classes[tup[2]].append((tup[0], tup[1]))
        return classes

    def map_power_word_to_class(self, word_tups):
        # Select the first tuple
        first_tup = word_tups[0]
        this_word = first_tup[1]
        # The name of this class
        this_class_name = self.power_words.word_to_class[this_word]
        this_class = self.power_words.name_to_class[this_class_name]
        return this_class

    def power_words_by_class_to_str(self, classes_counts):
        """Format the result of power_words_by_class to
        a readable string and return that string.
        """
        # Create dict to map power word to the class
        indent = " " * 4
        out = "\n"
        for aclass, word_tups in classes_counts.items():
            this_class = self.map_power_word_to_class(word_tups)
            out += "\n=== " + \
                    aclass + \
                    " - " + \
                    str(len(this_class.words)) + \
                    " total power words in this class ===\n"
            out += this_class.description + "\n"

            for atup in word_tups:
                cnt = atup[0]
                word = atup[1]
                out += indent + f"{cnt:5}" + " " + word + "\n"
        return out


def build_user_arg_dict(caller_argv):
    import textwrap

    parser = argparse.ArgumentParser(
            prog="Power Word Metrics",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog=textwrap.dedent('''\
                    Usage Examples:
                    power --help
                    power --input infile.txt
                    power -i infile.txt
                    '''))

    parser.add_argument("-i", "--input",
            type=str,
            action='store',
            required=True,
            help="User document to scan for power metrics")

    args = parser.parse_args(args=caller_argv[1:])   # skip prog name

    # Convert the argparser params into a standard dict
    # The resulting dict contains all the params of
    # the command line.
    user_arg_dict = {}
    for key, value in vars(args).items():
        user_arg_dict[key] = value

    return (user_arg_dict, parser)


def does_input_file_exist(filename):
    # Determine input file existence.
    # Return True  if the input file exist.
    #        False if the input file does not exist.
    if not os.path.isfile(filename):
        print(f'Required input file does not exists: {filename}')
        return False
    return True


def count_matches(power_word, text_buffer):
    """Given a text buffer, count the number of times
    the power_word is in text_buffer.

    Regex expressions get compiled by the library code.
    They also get cached whenever you use them so only
    WHEN when regex changes they get compiled.

    So:
        Pick a power word.
        Iterate the text buffer that read the entire
        text file.
        Ref: https://stackoverflow.com/questions/452104/is-it-worth-using-pythons-re-compile

    text_buffer = Line of text.
    patterns = The power word pattern.
        For each power word, enclose it with
        \b prefix
        \b suffix.

    Returns:
        The number of matches of power_word in text_buffer.  
    """
    pattern = r'\b' + power_word + r'\b'
    find_array = re.findall(pattern, text_buffer)
    return len(find_array)

def count_in_text(power_words, text_buffer):
    """Count the number of power words in the input text."""

    for aword in power_words.power_words_dict:
        cnt = count_matches(aword, text_buffer)
        if cnt != 0:
            power_words.power_words_dict[aword] += cnt
            class_name = power_words.word_to_class[aword]
            the_class = power_words.name_to_class[class_name]
            the_class.hits += 1

            #print(f"count {cnt}: {aword}")
    return power_words

def non_zero_power_words(counted_power_words_in_text):
    # Only non-zero counted power words
    the_items = counted_power_words_in_text.power_words_dict.items()
    counted_only = {key: value for (key, value) in the_items if value > 0}
    return counted_only

def main(argv):
    """Main routine to build power word report from
    user documents."""
    (user_arg_dict, parser) = build_user_arg_dict(argv)

    status = does_input_file_exist(user_arg_dict["input"])
    if not status:
        return status

    # Construct the structs necessary to process user input.
    power_words = AllPowerWords(all_power_word_classes)
    with open(user_arg_dict["input"], "r") as in_file:
        # All text is lower case and totally in core.
        text_buffer = in_file.read().lower()

    # Count all words
    pattern = r'\b\w+\b'
    all_array = re.findall(pattern, text_buffer)
    print(f"User power word count: {len(all_array)}" + \
            f" out of {len(power_words.power_words_dict)}" +\
            " power words")

    # Create counts for the user file by classes.
    counted_power_words_in_text = count_in_text(power_words, text_buffer)

    # Keep only power words with non-zero counts
    only_counted_words = non_zero_power_words(counted_power_words_in_text)

    # Sort by highest counts in each class.
    # sorted_counted looks something like:
    #    [('bloodbath', 3), ('assault', 2), ('blinded', 2), ...]
    # because keys must be unique and counts can be any int.
    sorted_counted = sorted(only_counted_words.items(), 
                            key=lambda kv: kv[1], reverse=True)

    out = ""
    the_report = Report(power_words, user_arg_dict, parser, sorted_counted)
    # Begin by summarizing each class, even if count == 0 for that class
    print("\nSummary by class for all classes of power words")
    counts_per_class = the_report.summary_by_class()
    the_report.print_summary_per_class(counts_per_class)

    # Display all the power words with most frequent first
    all_power_words_str = the_report.all_power_words_count()
    print(all_power_words_str)

    # Collect and display counts per class of power words
    classes_counts = the_report.power_words_by_class()
    by_classes = the_report.power_words_by_class_to_str(classes_counts)
    print(by_classes)


all_power_word_classes = []

fear = PowerWordClass(name="Provoke Fear",
    description="Fear is without a doubt the most powerful emotion for grabbing and keeping an audience’s attention.",
    words=[
        "Agony",
        "Lunatic",
        "Apocalypse",
        "Lurking",
        "Armageddon",
        "Massacre",
        "Assault",
        "Meltdown",
        "Backlash",
        "Menacing",
        "Beating",
        "Mired",
        "Beware",
        "Mistake",
        "Blinded",
        "Murder",
        "Blood",
        "Nightmare",
        "Bloodbath",
        "Painful",
        "Bloodcurdling",
        "Pale",
        "Bloody",
        "Panic",
        "Blunder",
        "Peril",
        "Bomb",
        "Piranha",
        "Buffoon",
        "Pitfall",
        "Bumbling",
        "Plague",
        "Cadaver",
        "Played",
        "Catastrophe",
        "Plummet",
        "Caution",
        "Plunge",
        "Collapse",
        "Poison",
        "Corpse",
        "Poor",
        "Crazy",
        "Prison",
        "Cripple",
        "Pummel",
        "Crisis",
        "Pus",
        "Danger",
        "Reckoning",
        "Dark",
        "Refugee",
        "Deadly",
        "Revenge",
        "Death",
        "Risky",
        "Deceiving",
        "Scary",
        "Destroy",
        "Scream",
        "Devastating",
        "Searing",
        "Disastrous",
        "Shame",
        "Doom",
        "Shatter",
        "Drowning",
        "Shellacking",
        "Dumb",
        "Shocked",
        "Embarrass",
        "Silly",
        "Fail",
        "Slaughter",
        "Feeble",
        "Slave",
        "Fired",
        "Strangle",
        "Fool",
        "Stupid",
        "Fooled",
        "Suicide",
        "Frantic",
        "Tailspin",
        "Frightening",
        "Tank",
        "Gambling",
        "Targeted",
        "Gullible",
        "Teetering",
        "Hack",
        "Terror",
        "Hazardous",
        "Terrorist",
        "Hoax",
        "Torture",
        "Holocaust",
        "Toxic",
        "Horrific",
        "Tragedy",
        "Hurricane",
        "Trap",
        "Injure",
        "Vaporize",
        "Insidious",
        "Victim",
        "Epidemic",
        "Cataclysmic",
        "Buffoon",
        "Suffering",
        "Reckoning",
        "Trauma",
        "Dangerous",
        "Annihilate",
        "Invasion",
        "Volatile",
        "IRS",
        "Vulnerable",
        "Jail",
        "Warning",
        "Jeopardy",
        "Nerd",
        "Lawsuit",
        "Wounded",
        "Looming",
        "Cringeworthy",
        "Last chance",
        "Fugacious",
        "Worry",
        "Wreaking havoc",
])

all_power_word_classes.append(fear)

energize = PowerWordClass(name = "Encourage and Energize",
    description = "Wake your readers from their stupor with words that infuse energy and enthusiasm.",
    words = [
        "Amazing",
        "Jubilant",
        "Ascend",
        "Legend",
        "Astonishing",
        "Life-changing",
        "Astounding",
        "Magic",
        "Audacious",
        "Marvelous",
        "Awe-inspiring",
        "Master",
        "Awesome",
        "Mind-blowing",
        "Backbone",
        "Miracle",
        "Badass",
        "Miraculous",
        "Beat",
        "Noble",
        "Belief",
        "Perfect",
        "Blissful",
        "Persuade",
        "Bravery",
        "Phenomenal",
        "Breathtaking",
        "Pluck",
        "Brilliant",
        "Power-up",
        "Celebrate",
        "Praise",
        "Cheer",
        "Prevail",
        "Colossal",
        "Remarkable",
        "Command",
        "Revel",
        "Conquer",
        "Rule",
        "Courage",
        "Score",
        "Daring",
        "Seize",
        "Defeat",
        "Sensational",
        "Defiance",
        "Spectacular",
        "Delight",
        "Spine",
        "Devoted",
        "Spirit",
        "Dignity",
        "Splendid",
        "Dominate",
        "Spunk",
        "Effortless",
        "Staggering",
        "Empower",
        "Strengthen",
        "Epic",
        "Striking",
        "Excellent",
        "Strong",
        "Excited",
        "Stunning",
        "Extraordinary",
        "Stunt",
        "Eye-opening",
        "Supreme",
        "Fabulous",
        "Surprising",
        "Faith",
        "Terrific",
        "Fantastic",
        "Thrive",
        "Fearless",
        "Thwart",
        "Ferocious",
        "Titan",
        "Fierce",
        "Tough",
        "Force",
        "Triumph",
        "Fulfill",
        "Tremendous",
        "Glorious",
        "Unbeatable",
        "Glory",
        "Unbelievable",
        "Graceful",
        "Unforgettable",
        "Grateful",
        "Unique",
        "Grit",
        "Unleash",
        "Guts",
        "Uplifting",
        "Happy",
        "Valiant",
        "Heart",
        "Valor",
        "Hero",
        "Vanquish",
        "Honor",
        "Victory",
        "Hope",
        "Win",
        "Incredible",
        "Wonderful",
        "Jaw-dropping",
        "Wondrous",
        "Kudos",
        "Brighten",
        "Lovable",
        "Radiant",
        "Flawless",
        "Classy",
        "Affable",
        "Stupendous",
        "Virtuoso",
        "Cheery",
        "Openhearted",
        "Jovial",
        "Beauteous",
        "Logophile",
        "Adorable",
])

all_power_word_classes.append(energize)

desire = PowerWordClass(name = "Elicit Desire",
    description = "Like it or not, sex sells, so tap into the provocative power of lust to lure your readers.",
    words = [
        "Allure",
        "Naughty",
        "Arouse",
        "Nude",
        "Bare",
        "Obscene",
        "Begging",
        "Orgasmic",
        "Beguiling",
        "Passionate",
        "Brazen",
        "Pining",
        "Captivating",
        "Pleasure",
        "Charm",
        "Provocative",
        "Cheeky",
        "Racy",
        "Climax",
        "Raunchy",
        "Crave",
        "Risque",
        "Delight",
        "Rowdy",
        "Delirious",
        "Salacious",
        "Depraved",
        "Satisfy",
        "Desire",
        "Saucy",
        "Dirty",
        "Scandalous",
        "Divine",
        "Seduce",
        "Ecstasy",
        "Seductive",
        "Embrace",
        "Sensual",
        "Enchant",
        "Sex",
        "Enthralling",
        "Shameless",
        "Entice",
        "Sinful",
        "Entrance",
        "Sleazy",
        "Excite",
        "Sleeping",
        "Explicit",
        "Spank",
        "Exposed",
        "Spellbinding",
        "Fascinate",
        "Spicy",
        "Forbidden",
        "Steamy",
        "Frisky",
        "Stimulating",
        "Goosebumps",
        "Strip",
        "Hanker",
        "Sweaty",
        "Heavenly",
        "Tantalizing",
        "Hottest",
        "Taste",
        "Hypnotic",
        "Tawdry",
        "Impure",
        "Tease",
        "Indecent",
        "Tempting",
        "Intense",
        "Thrilling",
        "Intoxicating",
        "Tickle",
        "Itching",
        "Tight",
        "Juicy",
        "Tingle",
        "Kinky",
        "Turn on",
        "Kiss",
        "Unabashed",
        "Lascivious",
        "Uncensored",
        "Lewd",
        "Untamed",
        "Lick",
        "Untouched",
        "Lonely",
        "Urge",
        "Longing",
        "Voluptuous",
        "Love",
        "Vulgar",
        "Pleasurable",
        "Charismatic",
        "Riveting",
        "Obsession",
        "Mouthwatering",
        "Compelling",
        "Magnetic",
        "Enchanting",
        "Lovely",
        "Engaging",
        "Intriguing",
        "Fascinating",
        "Flirt",
        "Alluring",
        "Lure",
        "Wanton",
        "Luscious",
        "Wet",
        "Lush",
        "Whip",
        "Lust",
        "Wild",
        "Mischievous",
        "X-rated",
        "Mouth-watering",
        "Yearning",
        "Naked",
        "Yummy",
        "Sneak peek",
        "Promiscuous",
])

all_power_word_classes.append(desire)

anger = PowerWordClass(name = "Incite Anger",
    description="Snap people from apathy to action using words that connect with your readers' anger.",
    words = [
        "Abhorrent",
        "Money-grubbing",
        "Abuse",
        "Nasty",
        "Annoying",
        "Nazi",
        "Arrogant",
        "No good",
        "Ass kicking",
        "Obnoxious",
        "Backstabbing",
        "Oppressive",
        "Barbaric",
        "Pain in the ass",
        "Bash",
        "Payback",
        "Beat down",
        "Perverse",
        "Big mouth",
        "Pesky",
        "Blatant",
        "Pest",
        "Brutal",
        "Phony",
        "Bullshit",
        "Pissed off",
        "Bully",
        "Pollute",
        "Cheat",
        "Pompous",
        "Clobber",
        "Pound",
        "Clown",
        "Preposterous",
        "Cocky",
        "Pretentious",
        "Corrupt",
        "Punch",
        "Coward",
        "Punish",
        "Crooked",
        "Rampant",
        "Crush",
        "Ravage",
        "Curse",
        "Repelling",
        "Debase",
        "Repugnant",
        "Defile",
        "Revile",
        "Delinquent",
        "Revolting",
        "Demolish",
        "Rotten",
        "Desecrate",
        "Rude",
        "Disgusting",
        "Ruined",
        "Dishonest",
        "Ruthless",
        "Distorted",
        "Savage",
        "Evil",
        "Scam",
        "Exploit",
        "Scold",
        "Force-fed",
        "Sick and tired",
        "Foul",
        "Fuc",
        "Shit face",
        "Coc",
        "Bon",
        "Bone",
        "Cock sucker",
        "Mother fucker",
        "Muther fucker",
        "Puss",
        "Sink",
        "Freaking out",
        "Slam",
        "Full of shit",
        "Slander",
        "Greedy",
        "Slap",
        "Gross",
        "Slay",
        "Harass",
        "Smash",
        "Hate",
        "Smear",
        "High and mighty",
        "Smug",
        "Horrid",
        "Sniveling",
        "Infuriating",
        "Snob",
        "Jackass",
        "Snooty",
        "Kick",
        "Snotty",
        "Kill",
        "Spoil",
        "Knock",
        "Stuck up",
        "Knock out",
        "Suck",
        "Know it all",
        "Terrorize",
        "Lies",
        "Trash",
        "Livid",
        "Trounce",
        "Loathsome",
        "Tyranny",
        "Loser",
        "Underhanded",
        "Lying",
        "Up to here",
        "Maul",
        "Useless",
        "Broke",
        "Stink",
        "Fear",
        "Raise hell",
        "Sneaky",
        "Screw",
        "Rant",
        "Miff",
        "Diatribe",
        "Vicious",
        "Weak",
        "Diminish",
        "Provoke",
        "Hostile",
        "Morally bankrupt",
        "Worst",
        "Thug",
        "B.S.",
        "BS",
        "Agitate",
        "Boil over",
        "Annoy",
        "Violent",
        "Misleading",
        "Violate",
        "Lollygag",
        "Quixotic",
])

all_power_word_classes.append(anger)

greed = PowerWordClass(name="Grapple with Greed",
    description= "Because whether they desire to make or save it, every human on the planet is interested in money.",
    words=[
        "Bank",
        "Jackpot",
        "Bargain",
        "Lowest price",
        "Best",
        "Luxurious",
        "Billion",
        "Marked down",
        "Bonanza",
        "Massive",
        "Booked solid",
        "Money",
        "Cash",
        "Money-draining",
        "Money draining",
        "Cheap",
        "Money-saving",
        "Money saving",
        "Costly",
        "Nest egg",
        "Discount",
        "Pay zero",
        "Dollar",
        "Prize",
        "Double",
        "Profit",
        "Explode",
        "Quadruple",
        "Extra",
        "Reduced",
        "Feast",
        "Rich",
        "Fortune",
        "Savings",
        "Don’t miss out",
        "Fast",
        "Giveaway",
        "While they last",
        "Instantly",
        "Expires",
        "Never again",
        "Premiere",
        "Final",
        "More",
        "Hurry",
        "Sale ends soon",
        "Value",
        "Monetize",
        "Big",
        "Save",
        "New",
        "Deadline",
        "Handsome",
        "Noteworthy",
        "Kick ass",
        "Moneymaking",
        "Knockout",
        "Lucky",
        "Notable",
        "Amplify",
        "At the top",
        "Attractive",
        "Wealthy",
        "Ahead of the game",
        "Legendary",
        "Beautiful",
        "Optimal",
        "Good-looking",
        "Successful",
        "Bold",
        "Fortunate",
        "Sassy",
        "Smart",
        "Elegant",
        "Gorgeous",
        "Clever",
        "Foxy",
        "Quick-witted",
        "Genius",
        "Effective",
        "Elite",
        "Drop-dead",
        "Crowned",
        "Dazzling",
        "You",
        "Turbo charge",
        "Bright",
        "Super-human",
        "Brassy",
        "Booming",
        "Boss",
        "Unbeaten",
        "Undefeated",
        "Boost",
        "Exclusive",
        "Frugal",
        "Special",
        "Price break",
        "Before",
        "Running out",
        "Upsell",
        "Bonus",
        "Free",
        "Six-figure",
        "Freebie",
        "Skyrocket",
        "Frenzy",
        "Soaring",
        "Prosperous",
        "Surge",
        "Gift",
        "Treasure",
        "Golden",
        "Triple",
        "Greatest",
        "Wasted",
        "High-paying",
        "High paying",
        "Wealth",
        "Inexpensive",
        "Whopping",
])

all_power_word_classes.append(greed)

security = PowerWordClass(name="Ensure Security",
    description="Choose words that make your readers feel safe, and you'll more easily gain their confidence and trust.",
    words=[
        "Above and beyond",
        "Privacy",
        "Anonymous",
        "Professional",
        "Authentic",
        "Protected",
        "Automatic",
        "Proven",
        "Backed",
        "Recession-proof",
        "Recession proof",
        "Bankable",
        "Refund",
        "Best-selling",
        "Reliable",
        "Cancel anytime",
        "Research",
        "Certified",
        "Results",
        "Clockwork",
        "Risk-free",
        "Endorsed",
        "Rock-solid",
        "Rock solid",
        "Foolproof",
        "Science-backed",
        "Guaranteed",
        "Scientific",
        "Moneyback",
        "Bona-fide",
        "Bona fide",
        "Recognized",
        "Authority",
        "Studies show",
        "Because",
        "Scientifically proven",
        "Genuine",
        "Worldwide",
        "Authoritative",
        "Safety",
        "Accredited",
        "Fully refundable",
        "Case study",
        "Well respected",
        "Dependable",
        "Improved",
        "Ensured",
        "Expert",
        "According to",
        "Track record",
        "Approved",
        "Ironclad",
        "Secure",
        "Legitimate",
        "Sure-fire",
        "Sure fire",
        "Lifetime",
        "Survive",
        "Money-back",
        "Money back",
        "Tested",
        "No obligation",
        "That never fails",
        "No questions asked",
        "Thorough",
        "No risk",
        "Trustworthy",
        "No strings attached",
        "Try before you buy",
        "No-fail",
        "No fail",
        "Unconditional",
        "Official",
        "Verify",
        "Permanent",
        "World-class",
        "Guilt-free",
        "Don’t worry",
])

all_power_word_classes.append(security)

temptation = PowerWordClass(name="Trigger Temptation",
    description="The truth is, we’re all fascinated by the mysterious and forbidden, so intrigue readers by sprinkling these power words throughout your writing.",
    words=[
        "Ancient",
        "Lost",
        "Backdoor",
        "Never seen before",
        "Banned",
        "Off the record",
        "Behind the scenes",
        "Off-limits",
        "Black Market",
        "Outlawed",
        "Blacklisted",
        "Private",
        "Bootleg",
        "Restricted",
        "Censored",
        "Sealed",
        "Remote",
        "Be the first",
        "Ridiculous",
        "Become an insider",
        "Secrets",
        "Bizarre",
        "Shocking",
        "Class full",
        "Ssshhh!!!",
        "Closet",
        "Spoiler",
        "Elusive",
        "Supersecret",
        "Super secret",
        "Hilarious",
        "Thought-provoking",
        "Thought provoking",
        "Illusive",
        "Top secret",
        "Incredibly",
        "Trade secret",
        "Insane",
        "Uncharted",
        "Interesting",
        "Unconventional",
        "Invitation only",
        "Undiscovered",
        "Key",
        "Unexplained",
        "Login required",
        "Signin required",
        "Unexplored",
        "Members only",
        "Unheard of",
        "Myths",
        "Unsung",
        "Odd",
        "Untold",
        "On the QT",
        "Unusual",
        "Priceless",
        "Wacky",
        "Privy",
        "Zany",
        "Psycho",
        "Classified",
        "Secret",
        "Cloak and dagger",
        "Smuggled",
        "Concealed",
        "Strange",
        "Confessions",
        "Tried to hide",
        "Confidential",
        "Unauthorized",
        "Controversial",
        "Uncensored",
        "Covert",
        "Under wraps",
        "Cover-up",
        "Cover-ups",
        "Cover up",
        "Cover ups",
        "Undercover",
        "Exotic",
        "Underground",
        "Forbidden",
        "Under-the-table",
        "Under the table",
        "Forgotten",
        "Undisclosed",
        "From the vault",
        "Unexpected",
        "Unlock",
        "Hush-hush",
        "Unreachable",
        "Illegal",
        "Unspoken",
        "Insider",
        "Unveiled",
        "Little-known",
        "Little known",
        "Withheld",
        "Hidden",
])

all_power_word_classes.append(temptation)

if __name__ == '__main__':
    sys.exit(main(sys.argv))

