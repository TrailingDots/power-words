Power Words Metrics
===================

This utility provides a metric for "power words"
in a text document. 

Bring your ideas to the front of your reader's
mind and cajole them to the way you think.
Use these compelling words to take action as you
use power words to persuade readers to take action.

While providing a simple count of power words,
using these words create a positive or negative
emotional response.

Sprinkle in a few power words and your writing
turns lifeless words into lively words.

See [801+ Power Words That Pack a Punch and Convert Like
Crazy](https://smartblogger.com/power-words/)
for a decent explanation of the definitions and uses.

# Installation

Download this project to a convenient directory.

Copy power/power.py to somewhere in your $PATH.
The copied location may be renamed to something
convenient such as "power".

A most likely location would be in $HOME/bin:

```bash
    # Copy and rename to "power"
    cp power/power.py $HOME/bin/power
    # Try it out
    power
    usage: Power Word Metrics [-h] -i INPUT
    Power Word Metrics: error: the following arguments are required: -i/--input
    # Try power on one of your documents
    power -i my_doc.txt
    ... whatever ...
```

