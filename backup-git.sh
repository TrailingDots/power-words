
#!/usr/bin/bash
#
# Script to create a compressed tar backup of the
# files in an application directory as well as
# run a git commit on the files.
# The notes typed in will be applied to both.
#
# Though not required, it’s a good idea to begin the commit message with a
# single short (less than 50 character) line summarizing the change, followed
# by a blank line and then a more thorough description.
#
# The backup name uses the top level directory
# as the application name.
#
# A backup filename appears as:
#   myApp-160226-091730.tgz
# where:
#   myApp   = application name
#   160226  = 2016 Feb 26 
#             Notice this sorts "ls myApp*" backups alphabetically.
#   091730  = 9:16 Hours 30 seconds. This is in 24 hour notation.
#

#set -x             # Uncomment for debugging
#PS4='$LINENO: '    # Uncomment for debugging


############# Clean up excess work files

# Get rid of any stray log and junk files. Ignore any error messages.
/usr/bin/rm $(find . -name '*.log') 2> /dev/null
/usr/bin/rm $(find . -name '*.pyc') 2> /dev/null
/usr/bin/rm $(find . -name 'typescript') 2> /dev/null

APP=$(basename $PWD)

DATE=$(date '+%y%m%d-%H%M%S')

# Place current commit note in temp file
TMP_NOTE=/tmp/tmp_note.$$
#
# Request a note for this version of backup
echo "-----------------------$APP-$DATE.tgz notes below------" >>NOTE.txt
echo Please enter a note for the reason for this backup.
echo The note may be of any length. Ctl-D to complete the note.
echo Backup note as of $DATE in $APP-$DATE.tgz>> NOTE.txt
cat >>$TMP_NOTE  # Collect input until a ^D

/bin/reset    # Because ^D does not seem to work well.

# Keep local copy of commit notes
cat $TMP_NOTE >>NOTE.txt

/usr/bin/git add -A    # Add all files for commit
/usr/bin/git status
/usr/bin/git commit --verbose --file $TMP_NOTE

# Clean up temp working notes.
#/usr/bin/rm $TMP_NOTE

############# Backup everything


(cd ..; \
    tar czf $APP-$DATE.tgz $APP; \
    ls -tl *.tgz)
        
