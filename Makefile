DESTDIR=/
PROJECT=mypower
BUILDDIR=$(CURDIR)/$(PROJECT)
LIBDIR=$(CURDIR)/$(PROJECT)
TOOLSDIR=$(CURDIR)/$(PROJECT)
LOCALDIR=$(HOME)/.local

PYTHON=`which python`

# Coverage requires special handling
COVERAGE=`which coverage`

PYTEST=`which pytest`

RM=/usr/bin/rm

RM=/usr/bin/rm
CP=/usr/bin/cp

all:
	make help

help:
	@echo "make help     - This help message."
	@echo "make coverage - Run test suite. Capture with 'script' command."
	@echo "make tests    - Run tests only."
	@echo "make wc       - Perform word count for line counts."
	@echo "make lint     - Perform extensive lint with prospector"

coverage:
	$(COVERAGE) run --branch -m unittest
	$(COVERAGE) report
	$(COVERAGE) html
	@echo
	@echo ============================================================
	@echo To view coverage load: file://$(PWD)/htmlcov/index.html
	@echo ============================================================

wc:
	./wc.sh

tests:
	${PYTHON} ${PYTEST}

# Users may not have all these nagging linters, but that's OK.
lint:
	@echo "Cyclomatic Complexity"
	radon cc $$(find . -name '*.py') --total-average --show-complexity
	@echo ""
	@echo "Maintainability Index"
	radon mi $$(find . -name '*.py') --show 
	@echo ""
	@echo "Raw metrics"
	radon raw $$(find . -name '*.py') --summary
	@echo ""
	@echo "Halstead complexity metrics"
	radon hal $$(find . -name '*.py') --functions
	@echo ""
	@echo "Prospector"
	prospector

