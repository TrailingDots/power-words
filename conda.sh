
#
# Do NOT use this as a shell script!
#
# MUST DOT this file as:
#     . conda.sh
#
# DOT this file to activate the correct
# virtual anaconda3 environment
#
# To see all virtual envs:
#     conda info -e
#
echo Change the anaconda3 environment to your
echo project environment!

conda activate py38

        
